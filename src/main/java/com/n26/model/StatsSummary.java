package com.n26.model;


import lombok.*;

import java.util.DoubleSummaryStatistics;

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
public class StatsSummary {
    private Double sum;
    private Double avg;
    private Double max;
    private Double min;
    private Long count;


    public StatsSummary(DoubleSummaryStatistics doubleSummaryStatistics){
        this.avg = doubleSummaryStatistics.getAverage();
        this.sum = doubleSummaryStatistics.getSum();
        this.count = doubleSummaryStatistics.getCount();
        this.max = doubleSummaryStatistics.getMax();
        this.min = doubleSummaryStatistics.getMin();
    }
}
