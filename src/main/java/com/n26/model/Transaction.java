package com.n26.model;


import com.n26.util.TimeHelperUtility;
import javaslang.control.Option;
import lombok.*;

@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {

    private static final int MAX_TIME_DIFFERENCE = 60;

    private Double amount;
    private Long timestamp;

    /**
     * @param currentMillis current time in milliseconds
     * @return check whether the transaction timestamp is from the last 60 seconds.
     */
    public Option<Transaction> isTxnTimeInLastSixtySeconds(long currentMillis){
        long diff = (currentMillis - timestamp)/ TimeHelperUtility.MILLIS_FOR_ONE_SECOND;
        return (diff > 0 && diff <= MAX_TIME_DIFFERENCE ? Option.of(this) : Option.none());
    }
}
