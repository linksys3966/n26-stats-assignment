package com.n26.controller;


import com.n26.model.StatsSummary;
import com.n26.service.StatsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatsController {

    private StatsService statsService;

    StatsController(StatsService statsService){
        this.statsService = statsService;
    }

    /**
     * @return get stats based on transactions happened in last 60 seconds
     */
    @RequestMapping(value = "/statistics", method = RequestMethod.GET)

    public StatsSummary getStatsSummary(){
        return statsService.getSummary();
    }
}
