package com.n26.service;


import com.n26.model.StatsSummary;
import com.n26.model.Transaction;
import com.n26.util.TimeHelperUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.DoubleSummaryStatistics;

@Slf4j
@Service
public class StatsService {

    private StatsCacheService statsCacheService;
    private TimeHelperUtility timeHelperUtility;
    private DoubleSummaryStatistics statsSummary;


    StatsService(StatsCacheService statsCacheService, TimeHelperUtility timeHelperUtility) {
        this.statsCacheService = statsCacheService;
        this.timeHelperUtility = timeHelperUtility;
        this.statsSummary = new DoubleSummaryStatistics();
    }


    /**
     * @return get stats based on transactions happened in last 60 seconds.If the cache is empty, returns the default stats
     */
    public StatsSummary getSummary() {
        if (statsCacheService.isCacheEmpty())
            return new StatsSummary(0.0, 0.0, 0.0, 0.0, 0L);
        return new StatsSummary(statsSummary);
    }


    /**
     * @param transaction
     * @return save the incoming transaction data and calculate the updated stats summary
     * At the same time, update the cache with the new transaction
     */
    StatsSummary saveStats(Transaction transaction) {

        // this operation is wrapped in synchronized block as statsSummary object is even accessed by the scheduled job
        // which is responsible for cleaning old transactions
        synchronized (this) {
            statsSummary.accept(transaction.getAmount());
        }

        StatsSummary updatedStatsSummary = new StatsSummary(statsSummary);

        // update the per second stats buffer with the new transaction
        statsCacheService.upsertStatsInCache(timeHelperUtility.convertTimeInMillisToSeconds(transaction.getTimestamp()), transaction.getAmount());

        return updatedStatsSummary;
    }

    /**
     * @return this is a scheduled job which runs every second to clean/prune transactions which are older than 60 seconds
     * from the current time. This is a async task which runs in a separate thread.
     */
    @Async
    @Scheduled(fixedDelay = TimeHelperUtility.MILLIS_FOR_ONE_SECOND, initialDelay = TimeHelperUtility.MILLIS_FOR_ONE_SECOND)
    public DoubleSummaryStatistics cleanOldStatsPerSecond() {
        long nowInSeconds = timeHelperUtility.currentSeconds();
        log.info("Job running to clean older transactions. current time: {}", nowInSeconds);
        long oldestTimeInSeconds = nowInSeconds - 60;

        // remove the entry from cache which is older than last sixty seconds if present which will return Option.Some on which
        // we recalculate the stats again, else if the timeKey is not present it returns None on which we do nothing
        return statsCacheService.removeFromCache(oldestTimeInSeconds)
                .map(a -> {
                    synchronized (this) {
                        log.info("ReCalculating stats summary");
                        statsSummary = statsCacheService.aggregate();
                    }
                    log.info("updated aggregated stats: {}", new StatsSummary(statsSummary));
                    return statsSummary;
                })
                .getOrElse(() -> {
                    log.info("stats older than 60 seconds not found for cleaning");
                    return statsSummary;
                });
    }

}

