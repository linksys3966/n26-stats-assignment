package com.n26.service;


import com.n26.model.StatsSummary;
import com.n26.model.Transaction;
import com.n26.util.TimeHelperUtility;
import javaslang.concurrent.Future;
import javaslang.control.Option;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class TransactionService {

    private StatsService statsService;

    private TimeHelperUtility timeHelperUtility;

    /**
     * @param statsService is responsible for persisting the data and calculating stats summary
     */
    TransactionService(StatsService statsService, TimeHelperUtility timeHelperUtility) {
        this.statsService = statsService;
        this.timeHelperUtility = timeHelperUtility;
    }


    /**
     * @param transaction
     * @return save transaction and return the statistics summary which is a Option<T>
     */
    public Option<StatsSummary> saveTransaction(Transaction transaction) {

        // check whether the transaction is from last sixty seconds which returns a Option<T>
        // if Option<T> is a success, asynchronously save the stats data using Future abstraction by passing it to stats service layer
        // if Option<T> is a None, signifies it is a older transaction, so return Option.none

        return transaction.isTxnTimeInLastSixtySeconds(timeHelperUtility.currentMillis())
                .flatMap(txn -> Future.of(() -> statsService.saveStats(txn)).toOption())
                .orElse(() -> {
                    log.info("Skipping transaction: {}", transaction);
                    return Option.none();
                });
    }
}
