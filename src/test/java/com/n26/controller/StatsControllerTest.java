package com.n26.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.n26.model.StatsSummary;
import com.n26.service.StatsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(MockitoJUnitRunner.class)
public class StatsControllerTest {


    @InjectMocks
    private StatsController statsController;

    @Mock
    private
    StatsService statsService;

    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        objectMapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(statsController).build();
    }

    @Test
    public void shouldReturnAggregatedStatsSummaryForTransactionsFromLast60Seconds() throws Exception {

        StatsSummary statsSummary = new StatsSummary(123.0, 124.5, 121.0, 119.0, 3L);
        when(statsService.getSummary()).thenReturn(statsSummary);

        mockMvc.perform(get("/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(statsSummary)));
    }

    @Test
    public void shouldReturnDefaultStatsSummaryIfThereAreNoTransactionsInCache() throws Exception {

        StatsSummary statsSummary = new StatsSummary(0.0, 0.5, 0.0, 0.0, 0L);
        when(statsService.getSummary()).thenReturn(statsSummary);

        mockMvc.perform(get("/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(statsSummary)));
    }
}