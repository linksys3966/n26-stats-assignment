package com.n26.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.n26.model.StatsSummary;
import com.n26.model.Transaction;
import com.n26.service.TransactionService;
import javaslang.control.Option;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.DoubleSummaryStatistics;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class TransactionControllerTest {

    private MockMvc mockMvc;

    @Mock
    private TransactionService transactionService;

    @InjectMocks
    private TransactionController transactionController;

    private ObjectMapper objectMapper;

    @Before
    public void setUp(){
        objectMapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(transactionController).build();
    }

    /**
     * @throws Exception
     */
    @Test
    public void shouldReturnHttpStatusAsCreatedForGivenTransactionIfSavedSuccessfully() throws Exception {

        Transaction validTransaction = new Transaction(44.0, 12345L);

        when(transactionService.saveTransaction(validTransaction)).thenReturn(Option.of(new StatsSummary(new DoubleSummaryStatistics())));

        mockMvc.perform(
                post("/transactions")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(validTransaction))).andExpect(status().isCreated());
    }

    /**
     * @throws Exception
     */
    @Test
    public void shouldReturnHttpStatusAsNoContentForGivenTransactionIfItsSkipped() throws Exception {

        Transaction validTransaction = new Transaction(44.0, 12345L);

        when(transactionService.saveTransaction(validTransaction)).thenReturn(Option.none());

        mockMvc.perform(
                post("/transactions")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(validTransaction))).andExpect(status().isNoContent());
    }
}