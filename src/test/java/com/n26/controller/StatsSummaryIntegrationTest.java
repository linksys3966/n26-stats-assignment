package com.n26.controller;


import com.n26.model.StatsSummary;
import com.n26.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static java.time.Instant.now;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StatsSummaryIntegrationTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void shouldBeAbleToSaveIncomingTransactionsToBufferAndGetStatisticsWhenRequired() throws Exception {

        submitNewTransactions();

        ResponseEntity<StatsSummary> statsRespnseBefore = getStatsSummaryAndAssertOutputsBeforeThreadSleep();

        Thread.sleep(3000);

        getStatsSummaryAfterThreadSleepAndAssertOutputs(statsRespnseBefore);


    }

    private void getStatsSummaryAfterThreadSleepAndAssertOutputs(ResponseEntity<StatsSummary> statsRespnseBefore) {
        ResponseEntity<StatsSummary> statsResponseAfter =
                testRestTemplate.getForEntity("/statistics", StatsSummary.class);
        assertThat(statsRespnseBefore.getStatusCode(), is(HttpStatus.OK));
        StatsSummary expectedStatsSummaryAfter = statsResponseAfter.getBody();
        assertThat(expectedStatsSummaryAfter.getCount(), is(2L));
        assertThat(expectedStatsSummaryAfter.getSum(), is(93.0));
        assertThat(expectedStatsSummaryAfter.getAvg(), is(46.5));
        assertThat(expectedStatsSummaryAfter.getMax(), is(50.0));
        assertThat(expectedStatsSummaryAfter.getMin(), is(43.0));
    }

    private ResponseEntity<StatsSummary> getStatsSummaryAndAssertOutputsBeforeThreadSleep() {
        ResponseEntity<StatsSummary> statsRespnseBefore =
                testRestTemplate.getForEntity("/statistics", StatsSummary.class);
        assertThat(statsRespnseBefore.getStatusCode(), is(HttpStatus.OK));
        StatsSummary expectedStatsSummaryBefore = statsRespnseBefore.getBody();
        assertThat(expectedStatsSummaryBefore.getCount(), is(3L));
        assertThat(expectedStatsSummaryBefore.getSum(), is(116.0));
        assertThat(expectedStatsSummaryBefore.getAvg(), is(38.666666666666664));
        assertThat(expectedStatsSummaryBefore.getMax(), is(50.0));
        assertThat(expectedStatsSummaryBefore.getMin(), is(23.0));
        return statsRespnseBefore;
    }

    private void submitNewTransactions() {
        Transaction txn4SecondsOlder = new Transaction(50.0, now().minusSeconds(4).toEpochMilli());
        ResponseEntity<Transaction> recentTxnOneResponse = testRestTemplate.postForEntity("/transactions", txn4SecondsOlder, Transaction.class);
        assertThat(recentTxnOneResponse.getStatusCode(), is(HttpStatus.CREATED));

        Transaction txn44SecondsOlder = new Transaction(43.0, now().minusSeconds(44).toEpochMilli());
        ResponseEntity<Transaction> recentTxnTwoResponse = testRestTemplate.postForEntity("/transactions", txn44SecondsOlder, Transaction.class);
        assertThat(recentTxnTwoResponse.getStatusCode(), is(HttpStatus.CREATED));

        Transaction txn58SecondsOlder = new Transaction(23.0, now().minusSeconds(58).toEpochMilli());
        ResponseEntity<Transaction> older61SecTxnResponse = testRestTemplate.postForEntity("/transactions", txn58SecondsOlder, Transaction.class);
        assertThat(older61SecTxnResponse.getStatusCode(), is(HttpStatus.CREATED));

        Transaction txn98SecondsOlder = new Transaction(29.0, now().minusSeconds(98).toEpochMilli());
        ResponseEntity<Transaction> older98SecTxnResponse = testRestTemplate.postForEntity("/transactions", txn98SecondsOlder, Transaction.class);
        assertThat(older98SecTxnResponse.getStatusCode(), is(HttpStatus.NO_CONTENT));
    }

}
