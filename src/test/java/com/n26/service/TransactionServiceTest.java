package com.n26.service;

import com.n26.model.StatsSummary;
import com.n26.model.Transaction;
import com.n26.util.TimeHelperUtility;
import javaslang.control.Option;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.DoubleSummaryStatistics;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    @Mock
    private TimeHelperUtility timeHelperUtilityMock;

    @Mock
    private StatsService statsService;


    @InjectMocks
    private
    TransactionService transactionService;

    @Test
    public void shouldSaveAValidTransactionSuccessfully() {

        Transaction validTransaction = new Transaction(43.8, 1525937802000L);
        when(timeHelperUtilityMock.currentMillis()).thenReturn(1525937817000L);
        when(statsService.saveStats(validTransaction)).thenReturn(new StatsSummary(new DoubleSummaryStatistics()));
        Option<StatsSummary> maybeStatSummary = transactionService.saveTransaction(validTransaction);
        assertTrue(maybeStatSummary.isDefined());
    }

    @Test
    public void shouldReturnNoneMonadAndNotSaveTheTransactionIfTransactionIsOlder() {

        Transaction inValidTransaction = new Transaction(46.8, 1525937802000L);
        when(timeHelperUtilityMock.currentMillis()).thenReturn(1525938021000L);
        verify(statsService, times(0)).saveStats(inValidTransaction);
        Option<StatsSummary> maybeStatSummary = transactionService.saveTransaction(inValidTransaction);
        assertFalse(maybeStatSummary.isDefined());
    }


}