# N26 Statistics Assignment

### Dependencies

1. Javaslang(https://github.com/vavr-io/vavr) library is used as it provides more functional abstractions over Java8 which boosts the code readability and ensures more type safety
2. Junit is used for testing purpose
3. Lombok is used in models
4. The project is built with ``maven`` as a dependency build tool

### Packaging and running locally

1. Run ``mvn clean install`` which cleans any previous builds, runs test cases and builds the package jar for this project which will stored under ``./target/n26-0.0.1-SNAPSHOT.jar``
2. Run ``java -jar ./target/n26-0.0.1-SNAPSHOT.jar`` from the root directory which explodes the jar and starts the web server.

### Running of application web server locally

1. Run ``mvn spring-boot:run`` which will start the application Tomcat web server listening on port 8080
2. All API's will be accessible from ``http://localhost:8080/``

### Running of test cases

1. Run ``mvn test`` which will run all the test cases


### List of API's

1. ``POST /transactions`` - saves the given transaction if valid for stats summary
2. ``GET /statistics`` - gets the aggregated summary statistics

### Code Flow

1. ``POST /transactions`` - This API accepts the incoming transaction data which is validated to check whether its a recent transaction.If its a valid
transaction, this is passed to the StatsService layer to store in cache asynchronously.
2. ``Scheduled JOB Transaction Cleaner`` - This is a scheduled job which runs per second to clean any transactions which are older than (currentTime - 60)seconds.
3. ``GET /statistics`` - This API returns the precalculated aggregated stats summary in O(1) constant time

### Explanation of individual components

1. ``StatsCacheService`` - This component is responsible for managing the stats buffer which stores the perSecondStats in a ConcurrentHashMap which
is of type ``<TransactionTimeInSeconds, StatsForGivenSecond>``. ConcurrentHashMap is used to allow concurrent updates to the map as this is accessed
both when incoming transaction request is placed as well as by the scheduled job which cleans old transactions.
2. ``StatsService`` - This component is responsible for managing the aggregated summary stats which saves it to aggregatedStatsSummary when there is
a new incoming transaction.This service then interacts with the ``StatsCacheService`` to store this stat in the buffer/cache. Updates to ``statsSummary``
object are wrapped in ``synchronized block`` as this object is accessible both when a new transaction takes place as well as by the async job scheduler
which cleans transactions.
3. ``TransactionService`` - This component accepts the incoming transaction which is then validated and passed to ``StatsService`` layer to save
asynchronously using ``Future<T>`` abstraction which submits the operation in a new thread. If the transaction is invalid it is skipped.

### Testing and Comments
1. Unit test cases are written for each component except models.
2. Integration test case is written which tests the whole flow of the application by submitting the transactions and then requesting the aggregated
stats summary. 
3. Comments are added wherever needed for public functions as there are monadic operations used from Javaslang library.
4. You need to turn on Annotation Processing option from IDE if you are viewing the code in Intellij Idea
   

 